// function f(x, y, z) { }
// var args = [0, 1, 2];
// console.log(f.apply(null, args));

// function g(x, y, z) { return x.concat(y).concat(z) }
// var args = ['a','b','c' ];
// console.log(g(...args));

let arrays  = [[3,5,6], ['a','b',6], [3,6]]
let numeros = (arrays
            .reduce((acc, arreglo) => acc.concat(arreglo),[])
            .filter(item => { return ! isNaN(item)
            })
            ).reduce((allNames, name)=> { 
                (name in allNames) ?  allNames[name]++ : allNames[name] = 1;
                return allNames;
            }, {});

// let numerosSinRepetir = numeros.reduce(function (allNames, name) { 
//     if (name in allNames) {
//       allNames[name]++;
//     }
//     else {
//       allNames[name] = 1;
//     }
//     return allNames;
//   }, {});
console.log(numeros);
//console.log(numerosSinRepetir);