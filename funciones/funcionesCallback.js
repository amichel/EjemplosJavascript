// Primera Forma funcion normal
//////////////////////// Comienzo
function sumaNormal (a,b){
    return a+b;
}
console.log(sumaNormal(10,10))

// funcion suma con ES6 ---> sumArrow = (a,b)=> {return a + b}  cuando los parametros son los mismos se obio los corchetes y el return y quedaria de esta forma 
sumaArrow= (a,b) => a+b
console.log(sumaArrow(7,7))
//////////////////// Fin

/////////////////  Funciones con callback /////////////////////////////
////// Comienzo 
sumaCallback = (a,b,callback)=> {       ////es lo mismo que function suma(a,b,callback) {
    return callback(a+b)        ////es lo mismo que       return callback(a+b)
}                               ////es lo mismo que  }                

// llamada a la callback incluyendo la funcion callback integrado
sumaCallback(5,5,(resultado)=>{          // Es lo mismo que  suma(5,5, function(resultado){
    console.log(resultado)       // Es lo mismo que         console.log(resultado)
})                               // Es lo mismo que  })
/////////////// Fin

//////////////// Comienzo 
sumar = (resultado)=>{
    console.log(resultado)
}
// la misma llamada a suma nada mas que crar la funcion sumar aparte y no tener todo embebido en el callback
/// ojo llamamos a suma en la linea 20 y ademas a callback en la lines 26
sumaCallback(5,5,sumar)
////////////////  Fin   
////////////////  Fin Callbacks ///////////////////////////////////////

///////// Promesas 
sumaPromise = (a, b)=>{
    return new Promise((resolve,reject) =>{
        resolve(a+b)
    })
}
sumaPromise(15,5).then(sumar)  /// forma corta reutilizando la callback sumar

sumaPromise(25,5).then((res)=>{ /// forma larga 
    console.log(res)
})

// En la Fucion de arriba de Promesas no tomamos en cuenta el reject en esta lo tomaremos para comprobacion de errores
sumaPromise2 = (a, b)=>{
    return new Promise((resolve, reject) =>{
        if (typeof a === 'number' && typeof b === 'number') {
            resolve(a+b)
        }
        else{
            reject('Error lo parametros deben ser Numericos')
        }
    })
}
sumaPromise2('50',100)  // para probar el error podemos insertar una literal en ves de uno de los numeros ej: sumaPromise2(50,'hola')
    .then(sumar)   /// forma corta reutilizando la callback sumar 
    .catch((error) =>{console.log(error)}) 