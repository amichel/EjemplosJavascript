// Arreglos uso de Reduce Map y Filter
// Ejemplo 1: Funcion que me devuelve la suma de los elementos del arreglo
let numerosArrego = [5,10,20]
let sumaNum = function(acumulador, numero){
    return acumulador+numero;
}
console.log(numerosArrego.reduce(sumaNum,0)); // Resultdo = 35


// Arreglos
// Ejemplo 2:
let arrays  = [[3,5,6], ['a','b',6], [3,6]]
// extraer solo los numeros de este arreglo de arreglos
let numeros = arrays
            .reduce((acc, arreglo) => acc.concat(arreglo),[])
            .filter(item => { return ! isNaN(item)
            }
);
// Reduce saca de mi array solo aquellos que son numeros            
let numerosSinRepetir = numeros.reduce(function (allNames, name) { 
    if (name in allNames) {
      allNames[name]++;
    }
    else {
      allNames[name] = 1;
    }
    return allNames;
  }, {}
);
// Imprimir en pantallas los numeros que no estan repetido en los arrays
// 
console.log(numeros);
console.log('numeroSinRepetir version  larga: ',numerosSinRepetir);

// version corta diferente a la de los numeros arriba
let numerosx = (arrays
    .reduce((acc, arreglo) => acc.concat(arreglo),[])
    .filter(item => { return ! isNaN(item)
    })
    ).reduce((allNames, name)=> { 
        (name in allNames) ?  allNames[name]++ : allNames[name] = 1;
        return allNames;
    }, {});
console.log('numeroSinRepetir version  Corta: ',numerosx);
console.log(Object.keys(numerosx));
